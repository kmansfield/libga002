/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
/*************************************
 *
 *************************************/

//#include <p24fxxxx.h>
#include "xc.h"
#define RTOSDEFINE
#include "rtos.h"
#define DEBUGDEFINE
#include "debug.h"

//#define EN_WDTCLEAR
// PIC24FJ64GA002 Configuration Bit Settings

// 'C' source line config statements

#include <xc.h>

// CONFIG2
#pragma config POSCMOD = NONE           // Primary Oscillator Select (Primary oscillator disabled)
#pragma config I2C1SEL = PRI            // I2C1 Pin Location Select (Use default SCL1/SDA1 pins)
#pragma config IOL1WAY = OFF            // IOLOCK Protection (Once IOLOCK is set, cannot be changed) OFF can be changed
#pragma config OSCIOFNC = ON            // Primary Oscillator Output Functions as RA3
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor (Clock switching and Fail-Safe Clock Monitor are disabled)
#pragma config FNOSC = FRCPLL           // Oscillator Select (Fast RC Oscillator with Postscaler (FRCDIV))
#pragma config SOSCSEL = SOSC           // Sec Oscillator Select (Default Secondary Oscillator (SOSC))
#pragma config WUTSEL = LEG             // Wake-up timer Select (Legacy Wake-up Timer)
#pragma config IESO = OFF               // Internal External Switch Over Mode (IESO mode (Two-Speed Start-up) disabled)

// CONFIG1
#pragma config WDTPS = PS8192           // Watchdog Timer Postscaler (1:8,192)
#pragma config FWPSA = PR32             // WDT Prescaler (Prescaler ratio of 1:32)
#pragma config WINDIS = OFF             // Watchdog Timer Window (Standard Watchdog Timer enabled,(Windowed-mode is enabled))
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (Watchdog Timer is enabled)
#pragma config ICS = PGx1               // Comm Channel Select (Emulator EMUC1/EMUD1 pins are shared with PGC1/PGD1)
#pragma config GWRP = OFF               // General Code Segment Write Protect (Writes to program memory are allowed)
#pragma config GCP = OFF                // General Code Segment Code Protect (Code protection is disabled)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG port is disabled)
 
void
RTOSInit(void)
{
   unsigned int i;
   unsigned int j;

   for (i=0; i<MAXQUEUE; i++)
   {
      runQueue[i] = 0;
      waitTimer1[i] = 0;
      waitADC[i] = 0;
      ADCState[i] = 0;
   }

   for (j=0; j<MAXEVENTS; j++)
      for (i=0; i<MAXQUEUE; i++)
      {
      waitEvent[j][i] = 0;
      eventState[j][i] = 0;
      }

   CLKDIVbits.RCDIV0 = 0;
   CLKDIVbits.RCDIV1 = 0;
   CLKDIVbits.RCDIV2 = 0;
   CLKDIVbits.DOZE2 = 0;
   CLKDIVbits.DOZE1 = 0;
   CLKDIVbits.DOZE0 = 0;
   //CLKDIVbits.PLLEN = 1;
   //CLKDIVbits.CPDIV0 = 0;
   //CLKDIVbits.CPDIV1 = 0;
   OSCCONbits.NOSC2 = 0;
   OSCCONbits.NOSC1 = 0;
   OSCCONbits.NOSC0 = 1;
   OSCCONbits.CLKLOCK = 0;
   OSCCONbits.OSWEN = 1;
   //while (OSCCONbits.LOCK!=1)
   //{
   //}

   //InitializeSystem();
}

//fp lastTask;

void
runNext(void)
{
  static unsigned int nextToRun;
  unsigned int i;

  i = 0;
  while (i < MAXQUEUE)
  {
    if (runQueue[nextToRun])
    {
        //lastTask = runQueue[nextToRun];
      (*runQueue[nextToRun])(runState[nextToRun]);
      runQueue[nextToRun] = 0;
      i = MAXQUEUE;
    }
    nextToRun++;
    if (nextToRun >= MAXQUEUE)
    {
      nextToRun = 0;
    }
    i++;
    
    asm("clrwdt");					// clear WDT
  }
}

int
verifyRun(fp task)
{
  int i;

  for (i=0; i<MAXQUEUE; i++)
  {
    if (runQueue[i] == task)
      return 1;
  }
  return 0;
}

void
runTask(fp task, int state)
{
   unsigned int i;

   i = 0;
   while (i < MAXQUEUE)
   {
      if (runQueue[i] == 0)
      {
         runQueue[i] = task;
         runState[i] = state;
         i = MAXQUEUE;
      }
      i++;
   }
}

void
declareTimer1(void)
{
   unsigned int i;

   for (i=0; i<MAXQUEUE; i++)
   {
      if (waitDelay[i])
      {
         waitDelay[i]--;
         if (waitDelay[i] == 0)
         {
             Nop();
            runTask(waitTimer1[i], waitState[i]);
            waitTimer1[i] = 0;
         }
      }
   }
}

void
declareADC(void)
{
   unsigned int i;

   for (i=0; i<MAXQUEUE; i++)
   {
      if (waitADC[i] != 0)
      {
         runTask(waitADC[i], ADCState[i]);
         waitADC[i] = 0;
         ADCState[i] = 0;
         AD1CON1bits.SAMP = 0;
      }
   }
}

int
verifyDelay(fp task, unsigned int length)
{
  int i;

  for (i=0; i<MAXQUEUE; i++)
  {
    if (waitTimer1[i] == task && length == waitDelay[i])
      return 1;
  }
  return 0;
}

int 
startADC(fp task, int chan, int state)
{
   unsigned int entry;
   for (entry=0; entry<MAXQUEUE; entry++)
   {
      if (waitADC[entry] == 0)
      {
         //SRbits.IPL = 7;
         waitADC[entry] = task;
         ADCState[entry] = state;
/*          
         //AD1CHS = chan;
        AD1CSSL = 0x0033;
        //AD1CON3 = 0x1F02; // Sample time = 31Tad, Tad = 3Tcy
          AD1CON2 = 0x0410;
        AD1CON1bits.ADON = 1; // turn ADC O  
        IFS0bits.AD1IF = 0; // clear ADC interrupt flag
        AD1CON1bits.ASAM = 1; // auto start sampling for 31Tad
         //AD1CON1bits.SAMP = 1;
 */
         AD1CON1 = 0x00E4;
         AD1CON2 = 0x0000;
         AD1CON3 = 0x1F02;

         AD1CHS = chan;
                  
         AD1CSSL = 0;
         _AD1IF = 0;       // clear interrupt flag
         _AD1IE = 1;       // enable ADC interrupt
        AD1CON1bits.ADON = 1; // auto start sampling for 31Tad
         //SRbits.IPL = 0;
         return 1;
      }
   }
   return 0;
}

int
delayMS(fp task,unsigned int length, int state)
{
   unsigned int entry;
   unsigned int availableEntry;

   availableEntry = 0;
   for (entry=0; entry<MAXQUEUE; entry++)
   {
     // find first available entry
     if (waitTimer1[entry] == 0 && availableEntry == 0)
     {
       availableEntry = entry;
     }
     // check to see if we already have this process scheduled
     if (waitTimer1[entry] == task)
     {
       break;
     }
   }
   // if we didn't find this task already waiting
   // put it in we have searched to the end of the list
   if (entry == MAXQUEUE)
   {
     waitDelay[availableEntry] = length;
     waitTimer1[availableEntry] = task;
     waitState[availableEntry] = state;
     return 1;
   }

   // This is the old loop that just found
   // an available entry and put the task in there
   // you can see that it can duplicate tasks on the
   // list
/*
   for (entry=0; entry<MAXQUEUE; entry++)
   {
      if (waitTimer1[entry] == 0)
      {
         //SRbits.IPL = 7;
         waitDelay[entry] = length;
         waitTimer1[entry] = task;
         //SRbits.IPL = 0;
         return 1;
      }
   }
*/
   return 0;
}

void
declareSPort2(void)
{
    if (waitSPort2)
        runTask(waitSPort2, stateSPort2);

    waitSPort2 = 0;
}

int
queueSPort2(fp task, int state)
{
   if (waitSPort2 == 0)
   {
      waitSPort2 = task;
      stateSPort2 = state;
      return 1;
   }
   return 0;
}

void
declareSPort1(void)
{
   if (waitSPort1)
   {
      runTask(waitSPort1, stateSPort1);
   }

   waitSPort1 = 0;
}

int
queueSerial1(fp task, int state)
{
   if (waitSPort1 == 0)
   {
      waitSPort1 = task;
      stateSPort1 = state;
      return 1;
   }
   return 0;
}

void
declareSTPort1(void)
{
    if (waitSTPort1)
    {
        runTask(waitSTPort1, stateSTPort1);
    }

    waitSTPort1 = 0;
}

int
queueSerial2(fp task, int state)
{
    if (waitSPort2 == 0)
    {
        waitSPort2 = task;
        stateSPort2 = state;
        return 1;
    }
    return 0;
}

void
declareSTPort2(void)
{
    if (waitSPort2)
        runTask(waitSPort2, stateSTPort2);

    waitSPort2 = 0;
}

void
declareUSB(void)
{
    if (waitUSB)
    {
        runTask(waitUSB, usbState);
    }

    waitUSB = 0;
}

int
queueUSB(fp task, int state)
{
   if (waitUSB == 0)
   {
      waitUSB = task;
      usbState = state;
      return 1;
   }
   return 0;
}

int
queueEvent(unsigned char eventNum, fp task, int state)
{
   unsigned int i;

   i = 0;
   while (i < MAXQUEUE)
   {

      if (waitEvent[eventNum][i] == 0)
      {
         waitEvent[eventNum][i] = task;
         eventState[eventNum][i] = state;
         return 1;
      }
   }
   return 0;
}

void
declareEvent(unsigned char event)
{
   unsigned int i;

   for (i=0; i<MAXQUEUE; i++)
   {
         if (waitEvent[event][i] != 0)
         {
            runTask(waitEvent[event][i], eventState[event][i]);
            waitEvent[event][i] = 0;
         }
   }
}

void
declareSI2CRead(void)
{
   if (waitSI2CRead)
      runTask(waitSI2CRead, i2cReadState);

   waitSI2CRead = 0;
}

int
eventSI2CRead(fp task, int state)
{
  waitSI2CRead = task;
   if (waitSI2CRead == 0)
   {
      waitSI2CRead = task;
      i2cReadState = state;
      return 1;
   }
   return 0;
}

void
declareSI2CWrite(void)
{
   if (waitSI2CWrite)
      runTask(waitSI2CWrite, i2cWriteState);

   waitSI2CWrite = 0;
}

int
eventSI2CWrite(fp task, int state)
{
  waitSI2CWrite = task;
   if (waitSI2CWrite == 0)
   {
      waitSI2CWrite = task;
      i2cWriteState = state;
      return 1;
   }
   return 0;
}

