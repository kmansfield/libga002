/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/

/*********************************************************
 *
 * file: initializeSystem.c 
 *
 * Author:  Kim Mansfield
 * Date: 3/29/2014
 * Copyright KMI Technology
 *
 * Synopsis: This routine provides the initialization for the
 * RTOS.  It includes an initialization for timer1 that
 * provides delays and other timing for the RTOS.
 *
 *********************************************************/

#include <p24fxxxx.h>
#include "rtos.h"
#include "pins.h"
#include "timer1.h"
#include "initializeSystem.h"

/*********************************************************
 * Routine: initializeSystem
 *
 * params: None
 * Returns: Nothing
 *
 * Synopsis: Calls timer1Init to initialize timer 1 to provide
 * a tick for the RTOS.  Then calls the RTOSInit routine to
 * get the RTOS ready to run.  You should call this at the
 * beginning of your program.
 * 
 * Example:
 *    initializeSystem();
 *
 *********************************************************/
void
initializeSystem(void)
{
   RTOSInit();
   timer1Init();
}

void
initSys(void)
{
   timer1Init();
   RTOSInit();
}
