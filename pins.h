/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
#ifndef PINSFIRST
#define PINSFIRST

void digitalWrite(unsigned char pin, unsigned char mode);
void pinMode(unsigned char pin, unsigned char mode);
int digitalRead(unsigned char pin);
void clearPullUp(unsigned char pin);
void setPullUp(unsigned char pin);
void setCNInt(unsigned char pin);
void clearCNInt(unsigned char pin);

#define PIN_LED1 1
#define PIN2 2
#define PIN2MASK 0x01
#define PIN2SHIFT 1
#define PIN3 3
#define PIN3MASK 0x02
#define PIN3SHIFT 2
#define PIN4 4
#define PIN4MASK 0x01
#define PIN4SHIFT 1
#define PIN5 5
#define PIN5MASK 0x02
#define PIN5SHIFT 2
#define PIN6 6
#define PIN6MASK 0x04
#define PIN6SHIFT 3
#define PIN7 7
#define PIN7MASK 0x08
#define PIN7SHIFT 4
#define PIN9 9
#define PIN9MASK 4
#define PIN9SHIFT 2
#define PIN10 10
#define PIN10MASK 8
#define PIN10SHIFT 3
#define PIN11 11
#define PIN11MASK 0x10
#define PIN11SHIFT 4
#define PIN12 12
#define PIN12MASK 0x10
#define PIN12SHIFT 4
#define PIN14 14
#define PIN14MASK 0x20
#define PIN14SHIFT 5
#define PIN15 15
#define PIN15MASK 0x40
#define PIN15SHIFT 6
#define PIN16 16
#define PIN16MASK 0x80
#define PIN16SHIFT 7
#define PIN17 17
#define PIN17MASK 0x100
#define PIN17SHIFT 8
#define PIN18 18
#define PIN18MASK 0x200
#define PIN18SHIFT 9
#define PIN21 21
#define PIN21MASK 0x400
#define PIN21SHIFT 10
#define PIN22 22
#define PIN22MASK 0x800
#define PIN22SHIFT 11
#define PIN23 23
#define PIN23MASK 0x1000
#define PIN23SHIFT 12
#define PIN24 24
#define PIN24MASK 0x2000
#define PIN24SHIFT 13
#define PIN25 25
#define PIN25MASK 0x4000
#define PIN25SHIFT 14
#define PIN26 26
#define PIN26MASK 0x8000
#define PIN26SHIFT 15


#define OUTPUT 0
#define INPUT 1

#define HIGH 1
#define LOW 0

#ifdef PINSDEFINE
#else
#endif

#endif
