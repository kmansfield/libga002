/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
#ifndef RTOSFIRST
#define RTOSFIRST

#define MAXQUEUE 16
#define MAXEVENTS 16
typedef void (*fp)(int);

void declareTimer1(void);
int delayMS(fp task,unsigned int length, int state);
void runTask(fp task, int state);
void runNext(void);
void RTOSInit(void);
int queueSerial1(fp task, int state);
void declareSPort1(void);
int queueSerial2(fp task, int state);
void declareSPort2(void);
void declareSTPort1(void);
int queueUSB(fp task, int state);
void declareUSB(void);
int eventSI2CWrite(fp task, int state);
void declareSI2CWrite(void);
int eventSI2CRead(fp task, int state);
void declareSI2CRead(void);
int verifyDelay(fp task, unsigned int length);
int verifyRun(fp task);
void declareADC(void);
int startADC(fp task,int chan, int state);

#ifdef RTOSDEFINE
fp runQueue[MAXQUEUE] = {0};
fp waitTimer1[MAXQUEUE] = {0};
unsigned int waitDelay[MAXQUEUE] = {0};
fp waitEvent[MAXEVENTS][MAXQUEUE];
fp waitADC[MAXQUEUE] = {0};

static int waitState[MAXQUEUE] = {0};
static int runState[MAXQUEUE] = {0};

//static int runEvent[MAXQUEUE] = {0};
static int ADCState[MAXQUEUE] = {0};
static int eventState[MAXEVENTS][MAXQUEUE];
static int stateSPort1 = 0;
static int stateSPort2 = 0;
static int stateSTPort1 = 0;
static int stateSTPort2 = 0;

static int usbState = 0;
static int i2cReadState = 0;
static int i2cWriteState = 0;

fp waitSPort1 = 0;
fp waitSPort2 = 0;
fp waitSTPort1 = 0;
fp waitUSB = 0;
fp waitSI2CRead = 0;
fp waitSI2CWrite = 0;
#else

#endif
#endif
