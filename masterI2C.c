/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/

/*********************************************************
 *
 * file: masterI2C.c 
 *
 * Author:  Kim Mansfield
 * Date: 3/29/2014
 * Copyright KMI Technology
 *
 * Synopsis: This routine provides support for the I2C
 * port.
 *
 *********************************************************/
#include "rtos.h"
#include "i2c.h"
#include "masterI2C.h"
#include "p24fxxxx.h"

typedef struct
{
   unsigned char rW;
   unsigned char address;
   unsigned char *location;
   unsigned char locLength;
   unsigned char *buffer;
   unsigned char length;
   unsigned char *error;
   fp task;
   int state;
} i2cStruct;

#define NOERROR 0
#define NOACK 1

i2cStruct i2cData[16];
unsigned int i2cPutPtr = 0;
unsigned int i2cGetPtr = 0;
unsigned int i2cCount = 0;
unsigned int i2cStarted = 0;
unsigned int i2cState = 0;

unsigned int bPtr = 0;

/*********************************************************
 * Routine: i2cQueue
 *
 * params: readWrite - what type of action to perform 1 is read
 *  0 is write
 *  address - address of the device to access. This is the 7
 *  bit address without the read/write bit.
 *  location - is the address to access inside the device.
 *  locLength - is the length of the location address.
 *  buffer - is the place where you put items to write or
 *  where items will be placed on read.
 *  length - length of the buffer
 *  error - is a place where errors from the I2C device are
 *  placed
 * Returns: returns 1 if completed successfully returns 0
 *  if it could queue the request.
 *
 * Synopsis: This takes a request to read or write an I2C
 * device.  This simply queues the request and starts it if
 * there is currently now I2C device activity.  This means
 * that your routine doesn't wait for the request to complete.
 * Your routine is free to do other things as I2C device
 * accesses can take a lot of time.
 * 
 * Example:
 *    unsigned char local[2];
 *    unsigned char lLength=2;
 *    unsigned char buffer[16];
 *    unsigned char err;
 *    if (i2cQueue(1,local,lLength,buff,16,&err) == 0)
 *      lcdWrite("Couldn't write to device);
 *
 *********************************************************/
unsigned int
i2cQueue(unsigned char readWrite, 
      unsigned char address, 
      unsigned char *location, 
      unsigned char locLength,
      unsigned char *buffer,
      unsigned char length,
      unsigned char *error,
      fp task,
      int state)
{
   unsigned int i2cTempPtr;

   i2cTempPtr = i2cPutPtr;
   i2cTempPtr++;
   i2cTempPtr &= 0x0f;

   if (i2cTempPtr == i2cGetPtr)
   {
      return 0;
   }

   //queue data
   i2cData[i2cPutPtr].rW = readWrite;
   i2cData[i2cPutPtr].address = address;
   i2cData[i2cPutPtr].location = location;
   i2cData[i2cPutPtr].locLength = locLength;
   i2cData[i2cPutPtr].buffer = buffer;
   i2cData[i2cPutPtr].length = length;
   i2cData[i2cPutPtr].error = error;
   i2cData[i2cPutPtr].task = task;
   i2cData[i2cPutPtr].state = state;

   i2cPutPtr++;
   i2cPutPtr &= 0x0f;
   i2cCount++;

   if (!i2cStarted)
   {
      I2C1CONbits.ACKDT = 0;	//Reset any previous Ack
      i2cStarted = 1;
      i2cState = 0;
      // start i2c
      I2C1CONbits.SEN = 1;	//Initiate Start condition
   }

   return 1;
}

// master interrupt
void __attribute__((interrupt,auto_psv)) _MI2C1Interrupt(void)
{
   switch (i2cState)
   {
      case 0:  // START bit sent
         if (!I2C1CONbits.SEN) // start bit
         //if (I2C1STAT & 0x0008) //start bit
         {
            // send address
            i2cState = 1;
            I2C1TRN = (i2cData[i2cGetPtr].address<<1) & 0xFE; // make it a write
         }
         break;
      case 1: // Address sent
         if (!I2C1STATbits.TRSTAT)  // transmit done?
         {
            if (I2C1STATbits.ACKSTAT == 1)
            {
               *(i2cData[i2cGetPtr].error) = NOACK;
               //runTask(i2cData[i2cGetPtr].task,i2cData[i2cGetPtr].state);
               // send stop we are done
               I2C1CONbits.RCEN = 0;
               I2C1STATbits.IWCOL = 0;
               I2C1STATbits.BCL = 0;
               I2C1CONbits.PEN = 1;	//Initiate Stop condition
               i2cState = 30;
            }
            else
            {
               bPtr = 0;
               I2C1TRN = i2cData[i2cGetPtr].location[bPtr++];
               i2cState = 10;
            }
         }
         break;
       case 10: // sent location
         if (!I2C1STATbits.TRSTAT)
         {
            if (I2C1STATbits.ACKSTAT == 0)
            {
               if (bPtr == i2cData[i2cGetPtr].locLength)
               {
                  if (i2cData[i2cGetPtr].rW)
                  {
                     // need restart then address
                     I2C1CONbits.ACKDT = 0;	//Reset any previous Ack
                     // Restart i2c
                     I2C1CONbits.RSEN = 1;	//Initiate Start condition
                     i2cState = 4;
                  }
                  else
                  {
                      if (i2cData[i2cGetPtr].length == 0)
                      {
                        // send stop we are done
                        I2C1CONbits.RCEN = 0;
                        I2C1STATbits.IWCOL = 0;
                        I2C1STATbits.BCL = 0;
                        I2C1CONbits.PEN = 1;	//Initiate Stop condition
                        i2cState = 3;   
                      }
                      else
                      {
                        I2C1TRN = i2cData[i2cGetPtr].buffer[0]; // make it a write
                        bPtr = 1;
                        i2cState = 2;
                      }
                  }
               }
               else
               {
                  I2C1TRN = i2cData[i2cGetPtr].location[bPtr++];
               }
            }
            else
            {
               // error
            }
         }
         break;
      case 2:  // sent byte to device
         if (!I2C1STATbits.TRSTAT)  // transmit done?
         {
            if (I2C1STATbits.ACKSTAT == 1)
            {
               *(i2cData[i2cGetPtr].error) = NOACK;
               //runTask(i2cData[i2cGetPtr].task,i2cData[i2cGetPtr].state);
               i2cGetPtr++;
               i2cGetPtr &= 0x0F;
               i2cCount--;
            }
            else
            {
               if (bPtr == i2cData[i2cGetPtr].length)
               {
                  // send stop we are done
                  I2C1CONbits.RCEN = 0;
                  I2C1STATbits.IWCOL = 0;
                  I2C1STATbits.BCL = 0;
                  I2C1CONbits.PEN = 1;	//Initiate Stop condition
                  i2cState = 3;
               }
               else
               {
                  I2C1TRN = i2cData[i2cGetPtr].buffer[bPtr++]; // make it a write
               }
            }
         }
         break;
      case 3:  // sent stop condition
         *(i2cData[i2cGetPtr].error) = NOERROR;
         runTask(i2cData[i2cGetPtr].task,i2cData[i2cGetPtr].state);
         i2cGetPtr++;
         i2cGetPtr &= 0x0F;
         i2cCount--;
         if (i2cCount)
         {
            I2C1CONbits.ACKDT = 0;	//Reset any previous Ack
            i2cStarted = 1;
            i2cState = 0;
            // start i2c
            I2C1CONbits.SEN = 1;	//Initiate Start condition
         }
         else
         {
            i2cStarted = 0;
         }
         break;
      case 4:  // sent restart
         if (!I2C1CONbits.RSEN)
         {
            I2C1TRN = (i2cData[i2cGetPtr].address<<1) | 0x01; // make it a read
            i2cState = 5;
         }
         else
         {
            // error
         }
         break;
      case 5:  // sent address
         if (!I2C1STATbits.TRSTAT)  // transmit done?
         {
            if (I2C1STATbits.ACKSTAT == 1)
            {
               // error

            }
            else
            {
               I2C1CONbits.RCEN = 1;
               bPtr = 0;
               i2cState = 6;
            }
         }
         else
         {
            // error
         }
         break;
      case 6:  // got byte from device
         if (I2C1STATbits.RBF)
         {
            i2cData[i2cGetPtr].buffer[bPtr++] = I2C1RCV;
            if (bPtr == i2cData[i2cGetPtr].length)
            {
               // NACK!!
               I2C1CONbits.ACKDT = 1;
               I2C1CONbits.ACKEN = 1;
               i2cState = 8;
            }
            else
            {
               // ACK
               I2C1CONbits.ACKDT = 0;
               I2C1CONbits.ACKEN = 1;
               i2cState = 7;
            }
         }
         else
         {
            // error
         }
         break;
      case 7:  // got byte from device read another
         I2C1CONbits.RCEN = 1;
         i2cState = 6;
         break;
      case 8: // got byte we are done send stop
         I2C1CONbits.RCEN = 0;
         I2C1STATbits.IWCOL = 0;
         I2C1STATbits.BCL = 0;
         I2C1CONbits.PEN = 1;	//Initiate Stop condition
         i2cState = 3;
         break;
      case 30:  // sent stop condition
         runTask(i2cData[i2cGetPtr].task,i2cData[i2cGetPtr].state);
         i2cGetPtr++;
         i2cGetPtr &= 0x0F;
         i2cCount--;
         if (i2cCount)
         {
            I2C1CONbits.ACKDT = 0;	//Reset any previous Ack
            i2cStarted = 1;
            i2cState = 0;
            // start i2c
            I2C1CONbits.SEN = 1;	//Initiate Start condition
         }
         else
         {
            i2cStarted = 0;
         }
         break;
  }
   IFS1bits.MI2C1IF = 0;	 // Clear Interrupt
}

// slave interrupt
void __attribute__((interrupt,auto_psv)) _SI2C1Interrupt(void)
{  
}

void DelayuSec(unsigned int cnt)
{
   while (cnt)
   {
      asm("nop");
      asm("nop");
      cnt--;
   }
}

/*********************************************************
 * Routine: i2cInit
 *
 * params: BRG - I2C clock rate
 * Returns: Nothing
 *
 * Synopsis: routine initiates I2C1 module to baud rate BRG
 *  You have the following speed ranges for the clock speed of the
 *  I2C device:
 *
 *  SPEED100K 0x9E    100khz
 *  SPEED400K 0x13    400khz
 *  SPEED1MHZ 0X07    1mhz
 * 
 * Example:
 *    i2cInit(SPEEDK100K);
 *
 *********************************************************/
void i2cInit(int BRG)
{
   int temp;

   i2cStarted = 0;
   // I2CBRG = 194 for 10Mhz OSCI with PPL with 100kHz I2C clock
   I2C1BRG = BRG;
   I2C1CONbits.I2CEN = 0;	// Disable I2C Mode
   I2C1CONbits.DISSLW = 1;	// Disable slew rate control
   IFS1bits.MI2C1IF = 0;	 // Clear Interrupt
   I2C1CONbits.I2CEN = 1;	// Enable I2C Mode
   temp = I2C1RCV;	 // read buffer to clear buffer full
   reset_i2c_bus();	 // set bus to idle
   //EnableIntI2C1;
   IEC1bits.MI2C1IE = 1;
}

//function iniates a start condition on bus
void i2c_start(void)
{
   int x = 0;
   I2C1CONbits.ACKDT = 0;	//Reset any previous Ack
   DelayuSec(10);
   I2C1CONbits.SEN = 1;	//Initiate Start condition
   Nop();

   //the hardware will automatically clear Start Bit
   //wait for automatic clear before proceding
   while (I2C1CONbits.SEN)
   {
      DelayuSec(1);
      x++;
      if (x > 20)
      break;
   }
   DelayuSec(2);
}

void i2c_restart(void)
{
   int x = 0;

   I2C1CONbits.RSEN = 1;	//Initiate restart condition
   Nop();
    
   //the hardware will automatically clear restart bit
   //wait for automatic clear before proceding
   while (I2C1CONbits.RSEN)
   {
      DelayuSec(1);
      x++;
      if (x > 20)	break;
   }
    
   DelayuSec(2);
}

//Resets the I2C bus to Idle
void reset_i2c_bus(void)
{
   int x = 0;

   //initiate stop bit
   I2C1CONbits.PEN = 1;

   //wait for hardware clear of stop bit
   while (I2C1CONbits.PEN)
   {
      DelayuSec(1);
      x ++;
      if (x > 20) break;
   }
   I2C1CONbits.RCEN = 0;
   IFS1bits.MI2C1IF = 0; // Clear Interrupt
   I2C1STATbits.IWCOL = 0;
   I2C1STATbits.BCL = 0;
   DelayuSec(10);
}

//basic I2C byte send
char send_i2c_byte(int data)
{
   int i;

   while (I2C1STATbits.TBF) { }
   IFS1bits.MI2C1IF = 0; // Clear Interrupt
   I2C1TRN = data; // load the outgoing data byte

   // wait for transmission
   for (i=0; i<500; i++)
   {
      if (!I2C1STATbits.TRSTAT) break;
      DelayuSec(1);

      }
      if (i == 500) {
      return(1);
   }

   // Check for NO_ACK from slave, abort if not found
   if (I2C1STATbits.ACKSTAT == 1)
   {
      reset_i2c_bus();
      return(1);
   }
   
   DelayuSec(2);
   return(0);
}

//function reads data, returns the read data, no ack
char i2c_read(void)
{
   int i = 0;
   char data = 0;

   //set I2C module to receive
   I2C1CONbits.RCEN = 1;

   //if no response, break
   while (!I2C1STATbits.RBF)
   {
      i ++;
      if (i > 2000) break;
   }

   //get data from I2CRCV register
   data = I2C1RCV;

   I2C1CONbits.ACKDT = 1;
   I2C1CONbits.ACKEN = 1;

   DelayuSec(10);

   while(I2C1CONbits.SEN || 
         I2C1CONbits.RSEN || 
         I2C1CONbits.PEN || 
         I2C1CONbits.RCEN ||
         I2C1CONbits.ACKEN || 
         I2C1STATbits.TRSTAT); 

   return data;
}

//function reads data, returns the read data, with ack
char i2c_read_ack(void)	//does not reset bus!!!
{
   int i = 0;
   char data = 0;

   //set I2C module to receive
   I2C1CONbits.RCEN = 1;

   //if no response, break
   while (!I2C1STATbits.RBF)
   {
      i++;
      if (i > 2000) break;
   }

   //get data from I2CRCV register
   data = I2C1RCV;

   //set ACK to high
   I2C1CONbits.ACKEN = 1;

   //wait before exiting
   DelayuSec(10);

   while(I2C1CONbits.SEN || 
         I2C1CONbits.RSEN || 
         I2C1CONbits.PEN || 
         I2C1CONbits.RCEN ||
         I2C1CONbits.ACKEN || 
         I2C1STATbits.TRSTAT); 

   return data;
}

void I2Cwrite(char addr, char subaddr, char value)
{
   i2c_start();
   send_i2c_byte(addr);
   send_i2c_byte(subaddr);
   send_i2c_byte(value);
   reset_i2c_bus();
}

char I2Cread(char addr, char subaddr)
{
   char temp;
   
   i2c_start();
   send_i2c_byte(addr);
   send_i2c_byte(subaddr);
   DelayuSec(10);

   i2c_restart();
   send_i2c_byte(addr | 0x01);
   temp = i2c_read();

   reset_i2c_bus();
   return temp;
}

unsigned char I2Cpoll(char addr)
{
   unsigned char temp = 0;

   i2c_start();
   temp = send_i2c_byte(addr);
   reset_i2c_bus();

   return temp;
}

