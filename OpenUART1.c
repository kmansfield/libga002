/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/

#include "uart.h"

#if defined (uart_v1_1) || defined (uart_v1_2) || defined (uart_v1_3) || defined (uart_v1_4) || defined (uart_v1_5 )|| defined (LIB_BUILD)
/*******************************************************************************
Function Prototype : void OpenUART1(unsigned int config1,unsigned int config2,
                                    unsigned int ubrg)
 
Include            : uart.h
 
Description        : This function configures the UART module
 
Arguments          : config1 - This contains the parameters to be configured in the
			UxMODE register as defined below	
			
			UART enable/disable					
				*	UART_EN					
				*	UART_DIS					
			UART Idle mode operation					
				*	UART_IDLE_CON					
				*	UART_IDLE_STOP				
			UART Wake-up on Start
				*	UART_EN_WAKE					
				*	UART_DIS_WAKE					
			UART Loopback mode enable/disable					
				*	UART_EN_LOOPBACK					
				*	UART_DIS_LOOPBACK					
			Input to Capture module					
				*	UART_EN_ABAUD					
				*	UART_DIS_ABAUD					
			Parity and data bits select					
				*	UART_NO_PAR_9BIT					
				*	UART_ODD_PAR_8BIT					
				*	UART_EVEN_PAR_8BIT					
				*	UART_NO_PAR_8BIT					
			Number of Stop bits					
				*	UART_2STOPBITS					
				*	UART_1STOPBIT
			IrDA encoder and decoder enable/disable
				*	UART_IrDA_ENABLE
				*	UART_IrDA_DISABLE
			Mode Selection 
				*	UART_MODE_SIMPLEX
				*	UART_MODE_FLOW
			Enable bits
				*	UART_UEN_11
				*	UART_UEN_10         
				*	UART_UEN_01
				*	UART_UEN_00 
			Receive Polarity Inversion bit
				*	UART_UXRX_IDLE_ZERO
				*	UART_UXRX_IDLE_ONE 
			High Baudrate Enable 
				*	UART_BRGH_FOUR 
				*	UART_BRGH_SIXTEEN				    
											
			config2 - This contains the parameters to be configured in the
			UxSTA register as defined below
			
			UART Transmission mode interrupt select					
				*	UART_INT_TX_BUF_EMPTY					
				*	UART_INT_TX_LAST_CH
				*	UART_INT_TX_EACH_INT8
			UART IrDA polarity inversion bit
				*	UART_IrDA_POL_INV_ONE
				*	UART_IrDA_POL_INV_ZERO    
			UART Transmit Break bit    
				*	UART_SYNC_BREAK_ENABLED
				*	UART_SYNC_BREAK_DISABLED				
			UART transmit enable/disable					
				*	UART_TX_ENABLE					
				*	UART_TX_DISABLE	
			UART Receive Interrupt mode select					
				*	UART_INT_RX_BUF_FUL					
				*	UART_INT_RX_3_4_FUL					
				*	UART_INT_RX_CHAR					
			UART address detect enable/disable					
				*	UART_ADR_DETECT_EN					
				*	UART_ADR_DETECT_DIS					
			UART OVERRUN bit clear					
				*	UART_RX_OVERRUN_CLEAR
			
			ubrg - This is the value to be written into UxBRG register 
			to set the baud rate.
			 
Return Value     :  None
 
Remarks          :  This functions configures the UART transmit and receive 
                    sections and sets the communication baud rate
*********************************************************************/
void OpenUART1(unsigned int config1,unsigned int config2, unsigned int ubrg)
{
    U1BRG  = ubrg;     /* baud rate */
    U1MODE = config1;  /* operation settings */
    U1STA = config2;   /* TX & RX interrupt modes */
    while(U1STAbits.URXDA == 1) /* Clear Buffer*/
        {
        char Temp;
        Temp = U1RXREG;
        }
    IFS0bits.U1RXIF = 0;         /*clear interrupt flag*/    
}

#else
#warning "Does not build on this target"
#endif
