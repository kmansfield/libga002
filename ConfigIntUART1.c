/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
 
#include "uart.h"

#if defined (uart_v1_1) || defined (uart_v1_2) || defined (uart_v1_3) || defined (uart_v1_4)|| defined (uart_v1_5 )|| defined (LIB_BUILD)
/**********************************************************************
Function Prototype : void ConfigIntUART1(unsigned int config)

Include            : uart.h
 
Description        : This function configures the UART Interrupts.
 
Arguments          : config - Individual interrupt enable/disable information 
                     as defined below
					 
		 Receive Interrupt enable					
			*	UART_RX_INT_EN					
			*	UART_RX_INT_DIS					
		 Receive Interrupt Priority					
			*	UART_RX_INT_PR0					
			*	UART_RX_INT_PR1				
			*	UART_RX_INT_PR2					
			*	UART_RX_INT_PR3					
			*	UART_RX_INT_PR4					
			*	UART_RX_INT_PR5					
			*	UART_RX_INT_PR6					
			*	UART_RX_INT_PR7					
		 Transmit Interrupt enable					
			*	UART_TX_INT_EN					
			*	UART_TX_INT_DIS					
		 Transmit Interrupt Priority					
			*	UART_TX_INT_PR0					
			*	UART_TX_INT_PR1					
			*	UART_TX_INT_PR2					
			*	UART_TX_INT_PR3					
			*	UART_TX_INT_PR4					
			*	UART_TX_INT_PR5					
			*	UART_TX_INT_PR6					
			*	UART_TX_INT_PR7
 
Return Value       : None
 
Remarks            : This function enables/disables the UART transmit and
                     receive interrupts and sets the interrupt priorities.
**********************************************************************/


void ConfigIntUART1(unsigned int config)
{
    /* clear IF flags */
    IFS0bits.U1RXIF = 0;
    IFS0bits.U1TXIF = 0;

    /* set priority */
    IPC2bits.U1RXIP = 0x0007 & config;
    IPC3bits.U1TXIP = (0x0070 & config) >> 4;

    /* enable/disable interrupt */
    IEC0bits.U1RXIE = (0x0008 & config) >> 3;
    IEC0bits.U1TXIE = (0x0080 & config) >> 7;
}

#else
#warning "Does not build on this target"
#endif
