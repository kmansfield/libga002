* Quick summary

This repository contains a library of routines for the PIC24FJ64GA002.  The library has a built in single stack RTOS and support for various peripherals contained on the chip.  Some of the supported peripherals include the UART, I2C (in master mode), ADC, PWM, and others.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Beta

### How do I get set up? ###

* Summary of set up

you simply build the library as a library and include it in the project you want to use it in.  

* Configuration

There is no formal configuration but, you can remove peripherals from the library if you aren't using them and when you want to use them you can include them again.  Of course, you have to rebuild the library each time you make a change like this.

* Dependencies

No dependencies at this time.

* Database configuration

No database at this time.

* How to run tests

To be determined.

* Deployment instructions

None

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Kim Mansfield (kmansfie@yahoo.com)

* Other community or team contact