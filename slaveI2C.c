/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
#include <p24fxxxx.h>
#include "i2c.h"
#include "rtos.h"

void
slaveI2CInit(unsigned char add)
{
	mI2C1_SlaveClearIntr();		// clears the I2C Slave interrupt flag
	EnableIntSI2C1;			   // enables I2C Slave Interrupt
	
	//Enable channel
	//OpenI2C2( I2C_ON, 0 );		// BRG is not used for I2C slave

    I2C1BRG = 0;
    I2C1CON = 0xFBA0; // I2C_ON;

	//I2C2ADD = 0x33;	/* 0b0110011 Slave MCU address (customized)
	I2C1ADD = add;	/* 0b0110011 Slave MCU address (customized)
					 * 4 msb is 0110, customized as slave MCU
					 * 3 lsb is 011, customized as the first slave MCU
					 */
}

unsigned char dataBuffer[32];
unsigned char dataLength;
unsigned int bufPtr;
unsigned char inDataBuf[32];
unsigned char inBufPtr;

void
slaveI2CPut(unsigned char *data, unsigned int length)
{
  unsigned char i;

  i = 0;
  while ((i < length) && (i < 32))
  {
    dataBuffer[i] = data[i];
    i++;
  }
  dataLength = length;
}

void
slaveI2CGet(unsigned char *data, unsigned int length)
{
  unsigned char i;

  i = 0;
  while ((i < length) && (i < 32))
  {
    data[i] = inDataBuf[i];
    i++;
  }
}

unsigned char readWrite = 0;

void __attribute__((interrupt, no_auto_psv)) _SI2C1Interrupt(void)
{

   if (I2C2STATbits.P == 1) 
   {
      // declare stop
      if (readWrite)
      {
         // declare read wait
         declareSI2CRead();
      }
      else
      {
         // declare write wait
         declareSI2CWrite();
      }
   } 
   else if (I2C2STATbits.D_A==0) 
   {
      // address byte
      inBufPtr = 0;			// clear array to first
      bufPtr = 0;
      inDataBuf[inBufPtr] = SlavegetcI2C2();

      if (I2C2STATbits.R_W) 
      {
         readWrite = 1;
         // read from slave
         SlaveputcI2C2(dataBuffer[bufPtr++]); 
      }	
   } 
   else 
   {
      // data byte
      if (I2C2STATbits.R_W) 
      {
         readWrite = 1;
         // read from slave output
         SlaveputcI2C2(dataBuffer[bufPtr++]); 
      } else {
         readWrite = 0;
         // write to slave input
         inDataBuf[inBufPtr++] = SlavegetcI2C2();	// get one char from i2c buffer
      }
      __builtin_btg((unsigned int *)&LATB, 12);
   }

   mI2C2_SlaveClearIntr();		// clears the I2C Slave interrupt flag
}

