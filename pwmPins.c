/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
#include <p24Fxxxx.h>
//#include <p24FJ64GA002.h>

void
pwmOutPins(unsigned char pwmOut, unsigned char pin)
{
     //***********************************
     // Unlock Registers
     //***********************************
     asm volatile ("MOV #OSCCON, w1\n"
         "MOV #0x46,w2\n"
         "MOV #0x57,w3\n"
         "MOV.b w2,[w1]\n"
         "MOV.b w3,[w1]\n"
         "BCLR OSCCON,#6"
     );

     //if (pwmOut == 0)
     //{
     //   pwmOut = 1;
     //}
     switch (pwmOut)
     {
       default:
       case 0:
         switch (pin)
         {
           case 4: // RP0
             RPOR0bits.RP0R = 18;
             break;
           case 5: // RP1
             RPOR0bits.RP1R = 18;
             break;
           case 6: // RP2
             RPOR1bits.RP2R = 18;
             break;
           case 7: // RP3
             RPOR1bits.RP3R = 18;
             break;
           case 11: // RP4
             RPOR2bits.RP4R = 18;
             break;
           case 14: // RP5
             RPOR2bits.RP5R = 18;
             break;
           case 15: // RP6
             RPOR3bits.RP6R = 18;
             break;
           case 16: // RP7
             RPOR3bits.RP7R = 18;
             break;
           case 17: // RP8
             RPOR4bits.RP8R = 18;
             break;
           case 18: // RP9
             RPOR4bits.RP9R = 18;
             break;
           case 21: // RP10
             RPOR5bits.RP10R = 18;
             break;
           case 22: // RP11
             RPOR5bits.RP11R = 18;
             break;
           case 23: // RP12
             RPOR6bits.RP12R = 18;
             break;
           case 24: // RP13
             RPOR6bits.RP13R = 18;
             break;
           case 25: // RP14
             RPOR7bits.RP14R = 18;
             break;
           case 26: // RP15
             RPOR7bits.RP15R = 18;
             break;
         }
         break;
       case 1:
         switch (pin)
         {
           case 4: // RP0
             RPOR0bits.RP0R = 19;
             break;
           case 5: // RP1
             RPOR0bits.RP1R = 19;
             break;
           case 6: // RP2
             RPOR1bits.RP2R = 19;
             break;
           case 7: // RP3
             RPOR1bits.RP3R = 19;
             break;
           case 11: // RP4
             RPOR2bits.RP4R = 19;
             break;
           case 14: // RP5
             RPOR2bits.RP5R = 19;
             break;
           case 15: // RP6
             RPOR3bits.RP6R = 19;
             break;
           case 16: // RP7
             RPOR3bits.RP7R = 19;
             break;
           case 17: // RP8
             RPOR4bits.RP8R = 19;
             break;
           case 18: // RP9
             RPOR4bits.RP9R = 19;
             break;
           case 21: // RP10
             RPOR5bits.RP10R = 19;
             break;
           case 22: // RP11
             RPOR5bits.RP11R = 19;
             break;
           case 23: // RP12
             RPOR6bits.RP12R = 19;
             break;
           case 24: // RP13
             RPOR6bits.RP13R = 19;
             break;
           case 25: // RP14
             RPOR7bits.RP14R = 19;
             break;
           case 26: // RP15
             RPOR7bits.RP15R = 19;
             break;
         }
         break;
       case 2:
         switch (pin)
         {
           case 4: // RP0
             RPOR0bits.RP0R = 20;
             break;
           case 5: // RP1
             RPOR0bits.RP1R = 20;
             break;
           case 6: // RP2
             RPOR1bits.RP2R = 20;
             break;
           case 7: // RP3
             RPOR1bits.RP3R = 20;
             break;
           case 11: // RP4
             RPOR2bits.RP4R = 20;
             break;
           case 14: // RP5
             RPOR2bits.RP5R = 20;
             break;
           case 15: // RP6
             RPOR3bits.RP6R = 20;
             break;
           case 16: // RP7
             RPOR3bits.RP7R = 20;
             break;
           case 17: // RP8
             RPOR4bits.RP8R = 20;
             break;
           case 18: // RP9
             RPOR4bits.RP9R = 20;
             break;
           case 21: // RP10
             RPOR5bits.RP10R = 20;
             break;
           case 22: // RP11
             RPOR5bits.RP11R = 20;
             break;
           case 23: // RP12
             RPOR6bits.RP12R = 20;
             break;
           case 24: // RP13
             RPOR6bits.RP13R = 20;
             break;
           case 25: // RP14
             RPOR7bits.RP14R = 20;
             break;
           case 26: // RP15
             RPOR7bits.RP15R = 20;
             break;
         }
         break;
       case 3:
         switch (pin)
         {
           case 4: // RP0
             RPOR0bits.RP0R = 21;
             break;
           case 5: // RP1
             RPOR0bits.RP1R = 21;
             break;
           case 6: // RP2
             RPOR1bits.RP2R = 21;
             break;
           case 7: // RP3
             RPOR1bits.RP3R = 21;
             break;
           case 11: // RP4
             RPOR2bits.RP4R = 21;
             break;
           case 14: // RP5
             RPOR2bits.RP5R = 21;
             break;
           case 15: // RP6
             RPOR3bits.RP6R = 21;
             break;
           case 16: // RP7
             RPOR3bits.RP7R = 21;
             break;
           case 17: // RP8
             RPOR4bits.RP8R = 21;
             break;
           case 18: // RP9
             RPOR4bits.RP9R = 21;
             break;
           case 21: // RP10
             RPOR5bits.RP10R = 21;
             break;
           case 22: // RP11
             RPOR5bits.RP11R = 21;
             break;
           case 23: // RP12
             RPOR6bits.RP12R = 21;
             break;
           case 24: // RP13
             RPOR6bits.RP13R = 21;
             break;
           case 25: // RP14
             RPOR7bits.RP14R = 21;
             break;
           case 26: // RP15
             RPOR7bits.RP15R = 21;
             break;
         }
         break;
       case 4:
         switch (pin)
         {
           case 4: // RP0
             RPOR0bits.RP0R = 22;
             break;
           case 5: // RP1
             RPOR0bits.RP1R = 22;
             break;
           case 6: // RP2
             RPOR1bits.RP2R = 22;
             break;
           case 7: // RP3
             RPOR1bits.RP3R = 22;
             break;
           case 11: // RP4
             RPOR2bits.RP4R = 22;
             break;
           case 14: // RP5
             RPOR2bits.RP5R = 22;
             break;
           case 15: // RP6
             RPOR3bits.RP6R = 22;
             break;
           case 16: // RP7
             RPOR3bits.RP7R = 22;
             break;
           case 17: // RP8
             RPOR4bits.RP8R = 22;
             break;
           case 18: // RP9
             RPOR4bits.RP9R = 22;
             break;
           case 21: // RP10
             RPOR5bits.RP10R = 22;
             break;
           case 22: // RP11
             RPOR5bits.RP11R = 22;
             break;
           case 23: // RP12
             RPOR6bits.RP12R = 22;
             break;
           case 24: // RP13
             RPOR6bits.RP13R = 22;
             break;
           case 25: // RP14
             RPOR7bits.RP14R = 22;
             break;
           case 26: // RP15
             RPOR7bits.RP15R = 22;
             break;
         }
         break;
     }

     //***********************************
     // Lock Registers
     //***********************************
     asm volatile ("MOV #OSCCON, w1\n"
         "MOV #0x46,w2\n"
         "MOV #0x57,w3\n"
         "MOV.b w2,[w1]\n"
         "MOV.b w3,[w1]\n"
         "BCLR OSCCON,#6"
     );
}// end pinAssign
