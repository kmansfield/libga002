#ifndef NEOPIXEL
#define NEOPIXEL

struct color_
{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
};

typedef struct color_ Color;

#define NUMLEDS 2

void init_neopixel(unsigned char pin);
int set_pixel_hexcode(int led_num, unsigned long hexcode);
int set_pixel_color(int led_num, Color color);
int set_pixel_rgb(int led_num, unsigned char r, unsigned char g, unsigned char b);
void apply_color();
void clear_neopixel();
#endif