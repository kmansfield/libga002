/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
/************************************************
 *
 ************************************************/


#include <p24Fxxxx.h>

#define USE_AND_OR
#include "uart.h"
//#include "uart1.h"
#include "rtos.h"
#include "system.h"
#include "debug.h"


#define BUFLEN 128
unsigned char U1buf[BUFLEN];
unsigned int U1ptr;
unsigned int U1gptr;
unsigned int U1cnt;
unsigned char U1Tbuf[BUFLEN];
unsigned int U1Tptr;
unsigned int U1Tcnt;
unsigned int U1Tgptr;

#define CONFIG11 UART_EN|UART_IDLE_CON|UART_IrDA_DISABLE|UART_MODE_FLOW|UART_UEN_00|UART_DIS_WAKE|UART_DIS_LOOPBACK|UART_DIS_ABAUD|UART_NO_PAR_8BIT|UART_BRGH_FOUR|UART_1STOPBIT

#define CONFIG12 UART_TX_ENABLE

void
uart1Init(unsigned long brate)
{
   unsigned long tbaud;

   tbaud = FCY/(4ul * brate) - 1;
   tbaud = 34;
   OpenUART1(CONFIG11, CONFIG12, tbaud);
   ConfigIntUART1(UART_RX_INT_PR4 | UART_RX_INT_EN | UART_TX_INT_PR4 | UART_TX_INT_DIS);
   EnableIntU1RX;
   U1STA |= 0x8000;
   U1ptr = U1gptr = U1cnt = 0;
   U1Tptr = U1Tgptr = U1Tcnt = 0;
   IFS0bits.U1RXIF = 0;
} // end uart1Init

void __attribute__ ((__interrupt__,__auto_psv__)) _ISR _U1RXInterrupt(void)
{
   while (U1STAbits.URXDA) {
       U1buf[U1ptr++] = U1RXREG;
       U1ptr &= BUFLEN-1;
       if (U1ptr == U1gptr) {
           U1gptr++;
           U1gptr &= BUFLEN-1;
       } else {
           U1cnt++;
       }
   }
   declareSPort1();
   // clear interrupts?
   IFS0bits.U1RXIF = 0;
} // end U1RXInterrupt

void __attribute__ ((auto_psv)) _ISR _U1TXInterrupt(void)
{
   while (U1STAbits.UTXBF == 0 && U1Tcnt)
   {
      if (U1Tcnt)
      {
         U1TXREG = U1Tbuf[U1Tgptr++];
         U1Tgptr &= BUFLEN-1;
         U1Tcnt--;
         if (U1Tcnt == 0)
         {
         DisableIntU1TX;
         declareSTPort1();
         }
      }
      else
      {
         DisableIntU1TX;
         declareSTPort1();
      }
   }
   // clear interrupts?
   IFS0bits.U1TXIF = 0;
} // end U1TXInterrupt

int uart1IsChar(void)
{
   return U1cnt;
} // end uart1IsChar

int uart1GetChar(void)
{
   int result;

   result = 0;
   IEC0bits.U1RXIE = 0;
   if (U1cnt)
   {
       result = U1buf[U1gptr++];
       U1gptr &= BUFLEN-1;
       U1cnt--;
   }
   IEC0bits.U1RXIE = 1;
   return result;
} // end uart1GetChar

void
uart1put(unsigned char c)
{
   if (U1STAbits.UTXBF || U1Tcnt)
   {
      U1Tbuf[U1Tptr++] = c;
      U1Tptr &= BUFLEN-1;
      if (U1Tptr == U1Tgptr) {
         U1Tgptr++;
         U1Tgptr &= BUFLEN-1;
      } else {
         U1Tcnt++;
      }
      EnableIntU1TX;
   }
   else
   {
       U1TXREG = c;
   }
   IFS0bits.U1TXIF = 0;

} // end uart1put

void
uart1ps(char *st)
{
   int i;

   i = 0;
   while (st[i] && i < BUFLEN) {
       uart1put(st[i++]);
   }
} // end uart1ps

int
uart1FreeSpace(void)
{
  return BUFLEN - U1Tcnt;
}
