#include "neopixel.h"
#include "pins.h"

#include <xc.h>
#include <stdint.h>

static Color led_color_buffer[NUMLEDS];
static unsigned char neopixel_pin;

static void
neo_bit_1(void)
{
  digitalWrite(neopixel_pin, HIGH);
  Nop();
  Nop();
  Nop();
  Nop();
  Nop();
  Nop();
  Nop();
  Nop();
  digitalWrite(neopixel_pin, LOW);
}

static void
neo_bit_0(void)
{
   digitalWrite(neopixel_pin, HIGH);
   Nop();
   Nop();
   digitalWrite(neopixel_pin, LOW);
}

static void 
send_pixel_color(unsigned char color_byte)
{
  int index;
  for(index = 7; index >= 0; index++)
  {
    if(color_byte & (1 << index))
    {
      neo_bit_1();
    }
    else
    {
      neo_bit_0();
    }
  }
}

static void
neopixel_delay(unsigned int delValue)
{
   int i;

   for (i=0; i<(delValue); i++)
   {
      Nop();
      Nop();
      Nop();
      Nop();
      Nop();
      Nop();
      Nop();
      Nop();
   }
}

void
clear_neopixel()
{
  int led_index;
  for(led_index = 0; led_index < NUMLEDS; led_index++)
  {
    Color temp_color;
    temp_color.red = 0;
    temp_color.green = 0;
    temp_color.blue = 0;
    
    led_color_buffer[led_index] = temp_color;
  }
}

void 
init_neopixel(unsigned char pin)
{  
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  
  clear_neopixel();
  
  neopixel_pin = pin;
}


int
set_pixel_rgb(int led_num, unsigned char r, unsigned char g, unsigned char b)
{
  Color color;
  color.red = r;
  color.green = g;
  color.blue = b;
  
  return set_pixel_color(led_num, color);
}

int 
set_pixel_hexcode(int led_num, unsigned long hexcode)
{
  Color color;
  color.red   = (unsigned char) (hexcode >> 16) & 0xFF;
  color.green = (unsigned char) (hexcode >> 8)  & 0xFF;
  color.blue  = (unsigned char) hexcode & 0xFF;
  
  return set_pixel_color(led_num, color);
}

int 
set_pixel_color(int led_num, Color color_code)
{
  int result = 0;
  
  if(led_num < NUMLEDS)
  {
    led_color_buffer[led_num] = color_code;
  }
  else
  {
    result = -1;
  }
  
  return result;
}

void 
apply_color()
{
  int led_index;
  
  for(led_index = 0; led_index < NUMLEDS; led_index++)
  {
    send_pixel_color(led_color_buffer[led_index].red);
    send_pixel_color(led_color_buffer[led_index].green);
    send_pixel_color(led_color_buffer[led_index].blue);
  }
  
  neopixel_delay(10000);
}


