/******************************************************************
 *
 * Copyright (c) 2012-2015, KMI Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Any corrections or additions to this library must be submitted to Kim at
 *    kim at kimansfield dot com
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************/
#include <p24Fxxxx.h>

void
uart1Pins(unsigned char rx, unsigned char tx)
{
     //***********************************
     // Unlock Registers
     //***********************************
     asm volatile ("MOV #OSCCON, w1\n"
         "MOV #0x46,w2\n"
         "MOV #0x57,w3\n"
         "MOV.b w2,[w1]\n"
         "MOV.b w3,[w1]\n"
         "BCLR OSCCON,#6"
     );

     switch (rx)
     {
       case 4:   // RP0
        RPINR18bits.U1RXR = 0;
        break;
       case 5:   // RP1
        RPINR18bits.U1RXR = 1;
        break;
       case 6:   // RP2
        RPINR18bits.U1RXR = 2;
        break;
       case 7:   // RP3
        RPINR18bits.U1RXR = 3;
        break;
       case 11:  // RP4
        RPINR18bits.U1RXR = 4;
        break;
       case 14:  // RP5
        RPINR18bits.U1RXR = 5;
        break;
       case 15:  // RP6
        RPINR18bits.U1RXR = 6;
        break;
       case 16:  // RP7
        RPINR18bits.U1RXR = 7;
        break;
       case 17:  // RP8
        RPINR18bits.U1RXR = 8;
        break;
       case 18:  // RP9
        RPINR18bits.U1RXR = 9;
        break;
       case 21:  // RP10
        RPINR18bits.U1RXR = 10;
        break;
       case 22:  // RP11
        RPINR18bits.U1RXR = 11;
        break;
       case 24:  // RP13
        RPINR18bits.U1RXR = 13;
        break;
       case 25: // RP14
        RPINR18bits.U1RXR = 14;
        break;
       case 26: // RP15
        RPINR18bits.U1RXR = 15;
        break;
       default:
        break;
     }
     switch(tx)
     {
        case 4: // RP0
           RPOR0bits.RP0R = 3;
           break;
        case 5: // RP1
           RPOR0bits.RP1R = 3;
           break;
        case 6: // RP2
           RPOR1bits.RP2R = 3;
           break;
        case 7: // RP3
           RPOR1bits.RP3R = 3;
           break;
        case 11: // RP4
           RPOR2bits.RP4R = 3;
           break;
        case 14:
           RPOR2bits.RP5R = 3;
           break;
        case 15:
           RPOR3bits.RP6R = 3;
           break;
        case 16: // RP7
           RPOR3bits.RP7R = 3;
           break;
        case 17: // RP8
           RPOR4bits.RP8R = 3;
           break;
        case 18: // RP9
           RPOR4bits.RP9R = 3;
           break;
        case 24: // RP13
           RPOR6bits.RP13R = 3;
           break;
        case 25: // RP14
           RPOR7bits.RP14R = 3;
           break;
        case 26: // RP15
           RPOR7bits.RP15R = 3;
           break;
        //case 10:
        //   RPOR5bits.RP10R = 3;
        //   break;
        //case 11:
        //   RPOR5bits.RP11R = 3;
        //   break;
        //case 12:
           // NOTE: no RP12!!
        //   break;
        //case 13:
        //   RPOR6bits.RP13R = 3;
        //   break;
        //case 14:
        //   RPOR7bits.RP14R = 3;
        //   break;
        //case 15:
        //   RPOR7bits.RP15R = 3;
        //   break;
           // This is for GB004's
#if 0
        case 16:
           RPOR8bits.RP16R = 3;
           break;
        case 17:
           RPOR8bits.RP17R = 3;
           break;
        case 18:
           RPOR9bits.RP18R = 3;
           break;
        case 19:
           RPOR9bits.RP19R = 3;
           break;
        case 20:
           RPOR10bits.RP20R = 3;
           break;
        case 21:
           RPOR10bits.RP21R = 3;
           break;
        case 22:
           RPOR11bits.RP22R = 3;
           break;
        case 23:
           RPOR11bits.RP23R = 3;
           break;
        case 24:
           RPOR12bits.RP24R = 3;
           break;
        case 25:
           RPOR12bits.RP25R = 3;
           break;
#endif
        default:
           break;
     }

     //***********************************
     // Lock Registers
     //***********************************
     asm volatile ("MOV #OSCCON, w1\n"
         "MOV #0x46,w2\n"
         "MOV #0x57,w3\n"
         "MOV.b w2,[w1]\n"
         "MOV.b w3,[w1]\n"
         "BCLR OSCCON,#6"
     );
}// end pinAssign
